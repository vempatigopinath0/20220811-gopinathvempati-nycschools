package com.photon.interviewtest.nycdetails

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.photon.interviewtest.BaseActivity
import com.photon.interviewtest.RetrofitService
import com.photon.interviewtest.databinding.ActivityNycDetailsBinding
import com.photon.interviewtest.repo.MainRepository
import com.photon.interviewtest.viewmodel.MainViewModel
import com.photon.interviewtest.viewmodel.MainViewModelFactory

class NycDetailsActivity : BaseActivity() {
    private var dbn: String = ""
    lateinit var binding: ActivityNycDetailsBinding
    lateinit var viewModel: MainViewModel
    var adapter = NycDetailsAdapter()
    private val retrofitService = RetrofitService.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dbn = intent.getStringExtra("schoolId").toString()
        binding = ActivityNycDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel =
            ViewModelProvider(this, MainViewModelFactory(MainRepository((retrofitService)))).get(
                MainViewModel::class.java
            )

        binding.nycDetailsRv.adapter = adapter
        viewModel.errorMessage.observe(this) {
            hideProgressBar()
            Toast.makeText(this@NycDetailsActivity, it, Toast.LENGTH_SHORT).show()
        }

        viewModel.nycDetailsResponse.observe(this) {
            hideProgressBar()
            if (it.isEmpty()) {
                binding.errorTv.setVisible()
                binding.nycDetailsRv.setGone()
            } else {
                binding.errorTv.setGone()
                binding.nycDetailsRv.setVisible()
                adapter.updateData(it)
            }
        }
        apiCall {
            showProgressBar()
            viewModel.getNyCDetails(dbn)
        }
    }
}