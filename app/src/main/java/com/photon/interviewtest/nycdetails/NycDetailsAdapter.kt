package com.photon.interviewtest.nycdetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.photon.interviewtest.BaseActivity.Companion.setVisible
import com.photon.interviewtest.data.NycDetails
import com.photon.interviewtest.databinding.ItemNyclistBinding

class NycDetailsAdapter : RecyclerView.Adapter<NycDetailsAdapter.NycViewHolder>() {

    var nycDetails: List<NycDetails> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NycViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        val itemNyclistBinding = ItemNyclistBinding.inflate(inflater, parent, false)
        return NycViewHolder(itemNyclistBinding)
    }

    override fun onBindViewHolder(holder: NycViewHolder, position: Int) {
        val nycItem = nycDetails[position]
        holder.bind(nycItem)
    }

    override fun getItemCount() = nycDetails.size

    fun updateData(nycDetails: List<NycDetails>) {
        this.nycDetails = nycDetails
        notifyDataSetChanged()
    }

    class NycViewHolder(val itemNyclistBinding: ItemNyclistBinding) :
        RecyclerView.ViewHolder(itemNyclistBinding.root) {
        fun bind(nycItem: NycDetails) {
            itemNyclistBinding.nooftesttakestTv.setVisible()
            itemNyclistBinding.schoolNameTv.text = "School Name : ".plus(nycItem.schoolName)
//            val readAvg = nycItem.satCriticalReadingAvgScore?.toInt() ?: 0
//            val mathAvg = nycItem.satMathAvgScore?.toInt() ?: 0
//            val writeAvg = nycItem.satWritingAvgScore?.toInt() ?: 0
//            val average = (readAvg + mathAvg + writeAvg) / 3
            itemNyclistBinding.schoolName2Tv.text = "Average reading score : ".plus(nycItem.satCriticalReadingAvgScore)
            itemNyclistBinding.schoolSportsTv.text = "Average Maths score : ".plus(nycItem.satMathAvgScore)
            itemNyclistBinding.totalStudentsTv.text = "Average writing score : ".plus(nycItem.satWritingAvgScore)
            itemNyclistBinding.nooftesttakestTv.text = "No'of Sat Test Takers : ".plus(nycItem.numOfSatTestTakers)
        }

    }
}