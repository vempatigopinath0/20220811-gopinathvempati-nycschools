package com.photon.interviewtest

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.photon.interviewtest.databinding.ActivityMainBinding
import com.photon.interviewtest.repo.MainRepository
import com.photon.interviewtest.viewmodel.MainViewModel
import com.photon.interviewtest.viewmodel.MainViewModelFactory

class MainActivity : BaseActivity() {

    private val TAG = "MainActivity"
    lateinit var binding: ActivityMainBinding
    lateinit var viewModel: MainViewModel
    private val retrofitService = RetrofitService.getInstance()
    private val adapter = MainAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel =
            ViewModelProvider(this, MainViewModelFactory(MainRepository((retrofitService)))).get(
                MainViewModel::class.java
            )

        binding.nycListRv.adapter = adapter

        viewModel.nycListResponse.observe(this) {
            hideProgressBar()
            adapter.updateData(it)
        }

        viewModel.errorMessage.observe(this) {
            hideProgressBar()
            Toast.makeText(this@MainActivity, it, Toast.LENGTH_SHORT).show()
        }

        apiCall {
            showProgressBar()
            viewModel.getNYCList()
        }
    }
}