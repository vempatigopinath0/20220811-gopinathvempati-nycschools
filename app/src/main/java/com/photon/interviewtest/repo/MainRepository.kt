package com.photon.interviewtest.repo

import com.photon.interviewtest.RetrofitService

class MainRepository constructor(private val retrofitService: RetrofitService) {
    /*A repository defines data operations.
    These operations sometimes need parameters that define how to run them
    for eg: fetch list of NYC schools and details*/
    fun getNYCList() = retrofitService.getNYCList()
    fun getNYCDetails(data: MutableMap<String, String>) = retrofitService.getNYCDetails(data)
}