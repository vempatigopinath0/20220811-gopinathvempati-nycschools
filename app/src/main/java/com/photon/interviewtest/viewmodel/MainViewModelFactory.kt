package com.photon.interviewtest.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.photon.interviewtest.repo.MainRepository

class MainViewModelFactory constructor(private val repository: MainRepository) :
    ViewModelProvider.Factory {
    /*
    We can not create ViewModel on our own.
    We need ViewModelProviders utility provided by Android to create ViewModels
    But ViewModelProviders can only instantiate ViewModels with no arg constructor*/
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            MainViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("Not found viewmodel")
        }
    }


}