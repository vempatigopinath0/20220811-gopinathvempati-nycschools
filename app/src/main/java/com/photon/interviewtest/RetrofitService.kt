package com.photon.interviewtest

import com.photon.interviewtest.data.NycDetails
import com.photon.interviewtest.data.NycResponse
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.QueryMap
import java.io.IOException
import java.util.concurrent.TimeUnit


interface RetrofitService {

    @GET("s3k6-pzi2.json")
    fun getNYCList(): Call<List<NycResponse>>

    @GET("f9bf-2cp4.json")
    fun getNYCDetails(@QueryMap query: MutableMap<String, String>): Call<List<NycDetails>>

    companion object {
        var retrofitService: RetrofitService? = null
        lateinit var okHttpClient: OkHttpClient

        /*Creating single retrofit instance*/
        fun getInstance(): RetrofitService {
            if (retrofitService == null) {
                val okHttpBuilder = OkHttpClient.Builder()

                if (BuildConfig.DEBUG) {
                    val logging = HttpLoggingInterceptor()
                    logging.level = HttpLoggingInterceptor.Level.BODY
                    okHttpBuilder.addInterceptor(logging)
                }

                okHttpClient = okHttpBuilder
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .addInterceptor(SecurityInterceptor())
                    .build()

                val gsonBuilder = GsonBuilder()
                val gson = gsonBuilder.create()

                val retrofit = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl("https://data.cityofnewyork.us/resource/")
                    .client(okHttpClient)
                    .build()

                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }
    }


    class SecurityInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val mainRequest = chain.request()
            val builder = mainRequest.newBuilder()
            addCustomHeaders(builder)
            return chain.proceed(builder.build())
        }

        private fun addCustomHeaders(builder: Request.Builder) {
            builder.header(
                "5l9rpyfeqk9xhyar5hma188uc",
                "65nt2c98kz4jkl0rf2m957hgbyrw5a37d74b4wck0c53myiurz"
            )
        }
    }

}
